"""jeulee URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from registration import urls as registration_urls
from mainsite import urls as mainsite_urls
from admin_pages import urls as admin_pages_urls
from markdownx import urls as markdownx

urlpatterns = [
    path("admin/", admin.site.urls),
    path("admin_pages/", include(admin_pages_urls)),
    path("markdownx/", include(markdownx)),
    path("registration/", include(registration_urls)),
    path("", include(mainsite_urls)),
]
