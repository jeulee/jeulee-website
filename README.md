# jeulee-website

Website for the Jeulée

## Install

```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
cp jeulee/settings.[dev|prod].py jeulee/settings.py
$EDITOR jeulee/settings.py  # Use settings that match your usecase
./manage.py migrate
./manage.py collectstatic  # In production only
```
