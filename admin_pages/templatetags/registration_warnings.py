from django import template
from registration.models import Team, Contestant
from site_wide_settings.models import SiteWideSettings

register = template.Library()


@register.filter
def team_warnings(team):
    """ Returns a list of warnings concerning the given team """
    warnings = team.warnings()
    mapper = {
        "invalid_members": ("membres invalides", "Inscription incomplète pour certains membres"),
        "undercrowded": ("sous-peuplée", "Pas assez de membres"),
        "overcrowded": ("sur-peuplée", "trop de membres"),
    }
    return list(map(lambda x: mapper[x], warnings))


@register.filter
def contestant_warnings(contestant):
    """ Returns a list of warnings concerning the given contestant """
    warnings = contestant.warnings()
    mapper = {
        "email": ("email", "email non confirmé"),
        "meal": ("repas", "repas non choisis"),
    }
    return list(map(lambda x: mapper[x], warnings))

