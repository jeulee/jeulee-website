from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import UserPassesTestMixin
from django.utils import timezone
from django.db.models import Count

from registration.models import Team, Contestant, MealInfos
from site_wide_settings.models import SiteWideSettings


class UserIsStaffMixin(UserPassesTestMixin):
    """ Checks that the user is member of the organization team """

    def test_func(self):
        return self.request.user.is_staff


class IndexView(UserIsStaffMixin, TemplateView):
    """ Admin pages' homepage """

    template_name = "admin_pages/index.html"


class TeamCompositionView(UserIsStaffMixin, TemplateView):
    """ An admin view listing the current teams formed """

    template_name = "admin_pages/team_composition.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["teams"] = Team.objects.all()
        context["no_team"] = Contestant.objects.filter(team=None)
        return context


class MetricsView(UserIsStaffMixin, TemplateView):
    """ An admin view with some useful metrics """

    template_name = "admin_pages/metrics.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        config = SiteWideSettings.get_solo()
        context["time_till_event"] = config.event_time_start - timezone.now()

        contestants = Contestant.objects.all()
        context["registered_contestants"] = contestants.count()
        context["fully_registered_contestants"] = contestants.filter(
            email_confirmed=True
        ).count()
        context["missing_email"] = contestants.filter(email_confirmed=False).count()
        context["missing_meals"] = contestants.filter(meals=None).count()

        teams = (
            Team.objects.all()
            .prefetch_related()
            .annotate(member_count=Count("members"))
        )
        context["team_count"] = teams.count()
        context["teams_with_two"] = teams.filter(member_count__gte=2).count()
        context["teams_undercrowded"] = teams.filter(
            member_count__lt=config.undercrowded_threshold
        ).count()
        context["teams_overcrowded"] = teams.filter(
            member_count__gt=config.overcrowded_threshold
        ).count()
        teams_within_bounds = teams.filter(
            member_count__gte=config.undercrowded_threshold,
            member_count__lte=config.overcrowded_threshold,
        )
        context["teams_without_warning"] = len(
            list(filter(lambda x: not x.has_invalid_members(), teams_within_bounds))
        )

        context["teamless_contestants"] = contestants.filter(team=None).count()

        return context


class MealsView(UserIsStaffMixin, TemplateView):
    """ An admin view with some useful infos about the meals """

    template_name = "admin_pages/meals.html"

    def aggregate_meals(self):
        config = SiteWideSettings.get_solo()

        def get_meal_price(meal):
            if meal in MealInfos.HOT_MEALS:
                return config.meal_price
            if meal in MealInfos.BREAKFASTS:
                return config.breakfast_price
            if meal in MealInfos.BRUNCHS:
                return config.brunch_price
            return 0

        def gen_meal_row(name, price):
            return {
                "name": name,
                "count": 0,
                "standard_count": 0,
                "vegan_count": 0,
                "other_specials_count": 0,
                "other_specials_list": [],
                "unit_price": price,
                "budget": 0,
            }

        meals = [
            gen_meal_row(MealInfos.MEAL_NAMES[meal], get_meal_price(meal))
            for meal in MealInfos.MEAL_NAMES.keys()
        ]

        meal_infos = MealInfos.objects.filter(accepted_price=True)

        for meal_booking in meal_infos:
            meals_map = [
                getattr(meal_booking, meal_name)
                for meal_name in MealInfos.MEAL_NAMES.keys()
            ]
            for meal_id in range(len(meals_map)):
                if meals_map[meal_id]:
                    meals[meal_id]["count"] += 1
                    if meal_booking.meal_restrictions == "vegan":
                        meals[meal_id]["vegan_count"] += 1
                    elif meal_booking.meal_restrictions == "other":
                        meals[meal_id]["other_specials_count"] += 1
                        meals[meal_id]["other_specials_list"].append(
                            meal_booking.other_meal_restrictions
                        )

        for meal in meals:
            meal["budget"] = meal["unit_price"] * meal["count"]
            meal["standard_count"] = (
                meal["count"] - meal["vegan_count"] - meal["other_specials_count"]
            )

        total_row = gen_meal_row("Total", 0)
        for meal in meals:
            for label in [
                "count",
                "standard_count",
                "vegan_count",
                "other_specials_count",
                "budget",
            ]:
                total_row[label] += meal[label]

        meals.append(total_row)

        return meals

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["not_accepted_price"] = any(
            map(
                lambda x: x.number() > 0, MealInfos.objects.filter(accepted_price=False)
            )
        )
        context["meals"] = self.aggregate_meals()
        return context
