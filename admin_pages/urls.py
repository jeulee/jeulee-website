from django.urls import path

from . import views

app_name = "admin_pages"

urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path(
        "team_composition/",
        views.TeamCompositionView.as_view(),
        name="team_composition",
    ),
    path("metrics/", views.MetricsView.as_view(), name="metrics"),
    path("meals/", views.MealsView.as_view(), name="meals"),
]
