from django.db import models
from django.utils import timezone
from markdownx.models import MarkdownxField
from solo.models import SingletonModel
import datetime


def _get_default_event_time_start():
    """ Default value for the event start time """
    return timezone.make_aware(datetime.datetime(2020, 4, 24, 19))


def _get_default_event_time_theme_reveal():
    """ Default value for the event start time """
    return _get_default_event_time_start() + datetime.timedelta(hours=1)


def _get_default_event_time_end():
    """ Default value for the event end time """
    return timezone.make_aware(datetime.datetime(2020, 4, 26, 16))


def _get_default_event_time_betatest():
    """ Default value for the event betatest time """
    return timezone.make_aware(datetime.datetime(2020, 4, 26, 12))


def _get_default_meals_freeze_date():
    return _get_default_event_time_start() - datetime.timedelta(days=10)


class SiteWideSettings(SingletonModel):
    """ Site-wide, DB-stored settings, defined through the admin interface """

    class Meta:
        verbose_name = "Configuration générale du site"

    def __str__(self):
        return "Configuration générale du site"

    event_time_start = models.DateTimeField(
        verbose_name="Date de début de l'événement",
        default=_get_default_event_time_start,
    )
    event_time_theme_reveal = models.DateTimeField(
        verbose_name="Date de révélation du thème",
        default=_get_default_event_time_theme_reveal,
    )
    event_time_betatest = models.DateTimeField(
        verbose_name="Date du début de la phase de test des jeux",
        default=_get_default_event_time_betatest,
    )
    event_time_end = models.DateTimeField(
        verbose_name="Date de fin de l'événement", default=_get_default_event_time_end,
    )
    meals_freeze_date = models.DateField(
        verbose_name="Date de freeze des repas",
        help_text="Date à laquelle plus aucun repas ne peut être modifié",
        default=_get_default_meals_freeze_date,
    )

    meals_info_headtext = MarkdownxField(
        "Infos sur les repas",
        help_text="Affiché en haut du formulaire pour choisir ses repas.",
        default="** Mettre un texte sur les repas ici **",
    )

    meal_price = models.DecimalField(
        "Prix d'un repas", max_digits=5, decimal_places=2, default=2.00
    )
    breakfast_price = models.DecimalField(
        "Prix d'un petit-déjeuner", max_digits=5, decimal_places=2, default=1.00
    )
    brunch_price = models.DecimalField(
        "Prix du brunch", max_digits=5, decimal_places=2, default=3.00,
    )

    undercrowded_threshold = models.PositiveIntegerField(
        "Limite d'équipe sous-peuplée",
        help_text="Les équipes avec un nombre de membres strictement inférieur seront "
        "considérées comme sous-peuplées",
        default=3,
    )

    overcrowded_threshold = models.PositiveIntegerField(
        "Limite d'équipe surpeuplée",
        help_text="Les équipes avec un nombre de membres strictement superieur seront "
        "considérées comme sur-peuplées",
        default=6,
    )
