from django.apps import AppConfig


class SiteWideSettingsConfig(AppConfig):
    name = "site_wide_settings"
    verbose_name = "Configuration générale"
