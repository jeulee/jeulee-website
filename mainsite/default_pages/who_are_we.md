# <i class="fa fa-users" aria-hidden="true"></i> Qui sommes-nous ?

Ready, Make, Play! est un évènement organisé par des étudiant·e·s parisien·ne·s passionné·e·s de jeux divers et variés et désireu·x·ses d'expérimenter la création ludique.

Plus spécifiquement, cet évènement est une initiative du club Jeux de l'ENS, soutenue par le club Jeux des Mines.

Ready, Make, Play! est sous la responsabilité légale du [COF de l'ENS](https://cof.ens.fr). Il est en grande partie financé par [PSL](https://www.psl.eu/) via l'appel à initiatives étudiantes.

## <i class="fa fa-envelope" aria-hidden="true"></i> Nous contacter {#contact_us}

Pour toute question, vous pouvez contacter les organisateurs de Ready, Make, Play! par email à l'adresse : `ue.lsp.sne@xuejopser`{.antispam}  (attention au copier-coller)
