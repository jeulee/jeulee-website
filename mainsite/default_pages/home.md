# <i class="fa fa-cog fa-slow-spin" aria-hidden="true"></i> Ready, Make, Play!

*Ready, Make, Play!* est un évènement de création de jeux de société sur le modèle de la Game Jam pour les jeux vidéo : formez une équipe avec vos ami·e·s et nous vous acceuilerons pour concevoir un jeu de société en un week-end, autour d'un thème donné. Vous pourrez aussi le faire tester et repartir avec votre création.

Cette « Board Game Jam » unique en son genre, se déroulera dans les locaux de l'ENS Ulm. Nous vous fournirons tous le matériel nécessaire pour créer votre jeu, et vous pourrez manger sur place.

Pour participer, l'inscription est obligatoire, alors n'attendez plus, et **essayez-vous au game design** !