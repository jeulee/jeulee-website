# Charte de bonne conduite

Afin que Ready, Make, Play! soit agréable pour tout le monde
— participant·e·s, organisateur·ices et plus généralement toute personne
amenée à côtoyer l'événement —, la présente charte introduit des *règles
de conduite générales* à suivre en amont et tout au long de l'événement.

## Comportement attendu

Un comportement respectueux est attendu de toute personne côtoyant l'événement.
En particulier, les comportements abusifs ou agressifs, le harcèlement, la
discrimination ou plus généralement tout comportement *mettant mal à l'aise* une
autre personne sont à proscrire.

Dans le cadre particulier du jeu de plateau, milieu habituellement très
masculin, nous demandons aux participant·e·s de porter une attention toute
particulière au sexisme. Par exemple, les comportements nocifs suivants sont
souvent retrouvés :

* couper la parole, en particulier lorsqu'une femme explique quelque chose,
* se réapproprier des idées,
* faire preuve de condescendance, par exemple en expliquant plus longuement la
  même chose à une femme,
* réduire les personnages féminins à leur apparence dans un jeu,
* des phrases du type « tu peux pas comprendre », « c'est un truc de mecs », …

## Faire valoir cette charte

Si le comportement de quelqu'un vous a mis·e mal à l'aise, vous pouvez en parler
à n'importe quel·le organisateur·ice de l'événement. Cette personne a un devoir
d'anonymat : n'hésitez pas à lui préciser, sans besoin de justification, si vous
souhaitez que certain·e·s organisateur·ices ne soient pas mis au courant de
votre doléance, ou ne sachent pas de qui émane la doléance.

## Conséquences possibles

Les organisateur·ices décideront de la suite appropriée à donner à un
manquement à cette charte, constaté ou signalé.

Les conséquences peuvent aller d'un simple rappel à l'ordre privé à une
exclusion temporaire ou définitive de l'événement — sans remboursement des
divers frais engagés par læ participant·e.

Si le manquement concerne un·e organisateur·ice ou groupe d'organisateur·ices
précis·es, iels seront écarté·e·s des discussions concernant la suite à
donner.
