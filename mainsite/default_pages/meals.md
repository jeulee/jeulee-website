# Menus

Tout au long du week-end, notre équipe de cuisine bénévole préparera des repas
sur place, quasi-intégralement fait maison et avec une grosse proportion de bio
! Tous les repas seront végétariens, mais des options véganes sont possibles.
Même les plus gros appétits devraient y trouver leur compte ;)

*Note : les menus peuvent être amenés à changer d'ici à l'événement.*

## Vendredi soir

* Parmentier végétarien et légumes glacés
* Riz au lait

## Samedi midi

* Tartinades de légumes sur pain de campagne
* Pavés de semoule au fromage sautés et salade
* Fromage blanc crumblé aux fruits

## Samedi soir

* Pizza et coleslaw
* Fondant au chocolat

## Dimanche midi : brunch

* Muffins salés et sucrés
* Tartes salées et sucrées
* Frites
* Œufs brouillés
* Jus de fruits et boissons chaudes
