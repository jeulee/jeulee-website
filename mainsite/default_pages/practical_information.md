{% load static %}
# Infos pratiques

<div class="indexbar">
  <a href="#schedule"><span>Programme</span></a>
  <a href="#pricing"><span>Tarifs</span></a>
  <a href="#venue"><span>Comment venir ?</span></a>
</div>

## <i class="fa fa-clock-o" aria-hidden="true"></i> Programme {#schedule}

Ready, Make, Play! se déroulera pendant le **week-end du 24 au 26 avril 2020**.

| Horaire | Évènement |
| ---------- | ---------------- |
| {{ site_config.event_time_start|date:'l d, H\hi'|capfirst}} | Début de Ready, Make, Play!  |
| {{ site_config.event_time_theme_reveal|date:'l d, H\hi'|capfirst}} | Révélation du thème |
| {{ site_config.event_time_betatest|date:'l d, H\hi'|capfirst}} | Début de la phase de test des jeux |
| {{ site_config.event_time_end|date:'l d, H\hi'|capfirst}} | Fin de Ready, Make, Play! |

Révélation du thème
: A partir de la révélation du thème, vous pourrez commencer à réfléchir à votre jeu, nous vous servirons également le repas si vous l'avez pris et vous pourrez utiliser le matériel. Pensez à venir dès 19h pour qu'on vous attribue un poste de travail et pour vous familiariser avec le matériel.

Phase de test des jeux
: Des volontaires viendront tester votre jeu, il faudra donc avoir un prototype (à peu près) jouable. Participer au tests ne nécessite pas d'inscription, donc vous pouvez inviter vos amis !

## <i class="fa fa-money" aria-hidden="true"></i> Tarifs {#pricing}

Ready, Make, Play! est un évènement entièrement **gratuit**.

Cela dit, nous vous proposons en option de manger sur place à
**{{ site_config.meal_price }} €** le repas chaud,
**{{ site_config.breakfast_price }} €** le petit-déjeuner et
**{{ site_config.brunch_price}} €** le brunch du dimanche. Les repas doivent
être commandés au plus tard le
**{{ site_config.meals_freeze_date|date }}**.

Dans tous les cas, l'[inscription]({% url 'registration:register_start' %})
avant le début de l'évènement est *obligatoire*. Si nous venions à être victime
de notre succès, nous pourrions être forcé·e·s de fermer les inscriptions.

## <i class="fa fa-map-signs"></i> Comment venir ? {#venue}

Ready, Make, Play! se déroulera dans les locaux de l'École Normale Supérieure
de Paris, au *45 rue d'Ulm, 75005 Paris*.

<iframe id="map" src="https://www.openstreetmap.org/export/embed.html?bbox=2.334809303283692%2C48.8368567401711%2C2.3548936843872075%2C48.84636099015179&amp;layer=hot&amp;marker=48.841602029496684%2C2.344851493835449"></iframe>

Vous pouvez facilement venir en transport en commun en utilisant les arrêts
suivants :
<div class="public-transport-info">
	<div class="underground icon">
		<img src="{% static 'img/ratp/metro-7.svg' %}" alt="Metro 7">
	</div>
	<div class="underground stop">Place Monge</div>
	<div class="underground icon">
		<img src="{% static 'img/ratp/rer-B.svg' %}" alt="RER B">
	</div>
	<div class="underground stop">Luxembourg</div>
	<div class="bus icon">
		<img src="{% static 'img/ratp/bus-24.svg' %}" alt="Bus 24">
	</div>
	<div class="bus stop">École Normale Supérieure</div>
	<div class="bus icon">
		<img src="{% static 'img/ratp/bus-21.svg' %}" alt="Bus 21">
		<img src="{% static 'img/ratp/bus-27.svg' %}" alt="Bus 27">
	</div>
	<div class="bus stop">Feuillantines</div>
	<div class="bus icon">
		<img src="{% static 'img/ratp/noctilien-14.svg' %}" alt="Noctilien 14">
		<img src="{% static 'img/ratp/noctilien-21.svg' %}" alt="Noctilien 21">
		<img src="{% static 'img/ratp/noctilien-122.svg' %}" alt="Noctilien 122">
	</div>
	<div class="bus stop">Auguste Comte</div>
</div>
