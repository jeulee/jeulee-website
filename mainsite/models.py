from django.db import models
from django.urls import reverse
from markdownx.models import MarkdownxField


class MarkdownPage(models.Model):
    name = models.CharField(max_length=64)
    slug = models.SlugField(blank=True, unique=True)
    content = MarkdownxField()
    lastmod = models.DateField(auto_now=True)

    class Meta:
        verbose_name = "page Markdown"
        verbose_name_plural = "pages Markdown"
        ordering = ["slug"]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        if self.slug:
            return reverse('mainsite:md_page', args=[self.slug])
        else:
            return reverse('mainsite:home')
