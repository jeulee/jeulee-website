from django.urls import path
from django.contrib.sitemaps.views import sitemap

from .views import MarkdownTemplateView, RobotsView, MarkdownPagesSitemap

app_name = "mainsite"

sitemaps = {"md_pages": MarkdownPagesSitemap}

urlpatterns = [
    path("", MarkdownTemplateView.as_view(), {"slug": ""}, name="home"),
    path("<slug:slug>/", MarkdownTemplateView.as_view(), name="md_page"),
    path("robots.txt", RobotsView.as_view(), name="robots"),
    path("sitemap.xml", sitemap, {'sitemaps': sitemaps}, name="sitemap"),
]
