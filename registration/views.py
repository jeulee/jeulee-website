from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from django.views.generic.base import TemplateView, ContextMixin, RedirectView
from django.views.generic.edit import FormView, UpdateView

from django.contrib import messages

from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from shared.markdown import markdownify

from django.contrib.auth import login, logout
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import (
    LoginView as DjangoLoginView,
    PasswordResetView as DjangoPasswordResetView,
    PasswordResetDoneView as DjangoPasswordResetDoneView,
    PasswordResetConfirmView as DjangoPasswordResetConfirmView,
    PasswordResetCompleteView as DjangoPasswordResetCompleteView,
)
from shared.tokens import email_token_generator

from site_wide_settings.models import SiteWideSettings
from . import forms as registration_forms
from .models import Team, MealInfos


"""
Basic login/logout views
########################################
"""


class LoginView(DjangoLoginView):
    """ View used to log users in """

    template_name = "registration/auth/login.html"
    authentication_form = registration_forms.AuthenticationForm


class LogoutView(RedirectView):
    """ View used to log users out """

    permanent = False
    pattern_name = "mainsite:home"

    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            logout(self.request)
        messages.info(self.request, "Vous avez bien été déconnecté·e.")
        return super().get_redirect_url(*args, **kwargs)


class PasswordResetView(DjangoPasswordResetView):
    """ Reset users' passwords """

    template_name = "registration/auth/password_reset.html"
    success_url = reverse_lazy("registration:password_reset_done")
    email_template_name = "registration/auth/mail/password_reset.mail"
    form_class = registration_forms.PasswordResetForm


class PasswordResetDoneView(DjangoPasswordResetDoneView):
    """ Confirm the user their password reset request was completed """

    template_name = "registration/auth/password_reset_done.html"


class PasswordResetConfirmView(DjangoPasswordResetConfirmView):
    """ Prompt the user for a new password after reset procedure """

    template_name = "registration/auth/password_reset_confirm.html"
    success_url = reverse_lazy("registration:password_reset_complete")
    form_class = registration_forms.SetPasswordForm


class PasswordResetCompleteView(DjangoPasswordResetCompleteView):
    """ Confirm the user their password was duly reset """

    template_name = "registration/auth/password_reset_complete.html"


"""
Registration views
########################################
"""


class UserIsContestantMixin(LoginRequiredMixin, UserPassesTestMixin):
    """ Checks whether the user is a contestant (ie. has a linked `Contestant` model)
    """

    def test_func(self):
        return hasattr(self.request.user, "contestant")


class RegistrationMixin(ContextMixin):
    """ Mixin to be included in every registration view """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["is_registration"] = True
        return context


class MyRegistrationMixin(ContextMixin):
    """ Mixin to be included in every "my registration" view """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["is_my_registration"] = True
        return context


class RedirectConnectedUserMixin:
    """ Redirect users that are already connected to the main registration page """

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("registration:registration")
        return super().dispatch(request, *args, **kwargs)


class RegisterStartView(RegistrationMixin, RedirectConnectedUserMixin, TemplateView):
    """ Starting point for the registration procedure """

    template_name = "registration/register/start.html"

    def post(self, request, *args, **kwargs):
        """ Store the team type in the session, then proceeds to next step """
        team_type = request.POST["teamtype"]

        if team_type == "join_team":
            return redirect("registration:register_join_team_needs_link")
        if team_type in ["new_team", "without_team"]:
            request.session["register_team_type"] = team_type
            return redirect("registration:register_infos")
        raise PermissionDenied


class RegisterWithTeamNeedsLinkView(RegistrationMixin, TemplateView):
    """ Inform the user that to join a team, they must have the invite link """

    template_name = "registration/register/join_team_needs_link.html"


class RegisterInfosView(RegistrationMixin, RedirectConnectedUserMixin, FormView):
    """ Registration view to gather personnal infos """

    template_name = "registration/register/infos.html"
    success_url = reverse_lazy("registration:email_confirmation_needed")
    form_class = registration_forms.RegistrationPersonalInfosForm

    def handle_session_expired(self):
        """ Handle a session expiracy, either at page load time or at form submit time
        """
        messages.error(
            self.request,
            (
                "Votre session a expiré depuis le début de l'inscription. "
                "Veuillez recommencer, en re-cliquant sur le lien d'invitation si "
                "vous aviez été invité·e dans une équipe."
            ),
        )
        return redirect("registration:register_start")

    def has_session_expired(self):
        """ Check whether the required infos are still in the user's session """
        if "register_team_type" not in self.request.session:
            return True
        team_type = self.request.session["register_team_type"]
        if team_type not in ["new_team", "without_team", "join_team"]:
            return True
        if team_type == "join_team":
            if "register_team_join_token" not in self.request.session:
                return True
        return False

    def dispatch(self, *args, **kwargs):
        """ Check that the team type is well set """

        if self.has_session_expired():
            # Either the session expired, or someone tries to crash the process
            return self.handle_session_expired()

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        """ Create the User and Contestant objects """
        if self.has_session_expired():
            return self.handle_session_expired()

        contestant = form.create_contestant()
        login(self.request, contestant.user)

        register_team_type = self.request.session["register_team_type"]
        if register_team_type == "without_team":
            contestant.team = None
        elif register_team_type == "new_team":
            team = Team.objects.create()
            contestant.team = team
        elif register_team_type == "join_team":
            try:
                team = Team.objects.get(
                    invite_token=self.request.session["register_team_join_token"]
                )
            except Team.DoesNotExist:
                return self.handle_session_expired()  # kind of.
            contestant.team = team
        else:
            # Cannot happen? This is a server error, at this point, as validation
            # should have ruled out this form before.
            raise Exception(
                "`register_team_type` should be either 'without_team' or 'new_team'"
            )

        contestant.save()

        return redirect("registration:email_confirmation_needed")


class EmailConfirmationNeededView(
    MyRegistrationMixin, UserIsContestantMixin, TemplateView
):

    template_name = "registration/email_confirmation_needed.html"

    def get(self, request, *args, **kwargs):
        user = self.request.user
        contestant = user.contestant
        if contestant.email_confirmed:
            return redirect("registration:registration")

        subject = "Inscription à Ready, Make, Play!"
        message = render_to_string(
            "registration/confirm_email.mail",
            {
                "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                "token": email_token_generator.make_token(user),
            },
            request,
        )
        user.email_user(subject, message)

        return super().get(self, request, *args, **kwargs)


class ConfirmEmailView(RedirectView):
    """ View used to confirm somebody's email """

    permanent = False

    def get_redirect_url(self, uidb64, token, *args, **kwargs):
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
            contestant = user.contestant
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            messages.error(
                self.request,
                "Le lien de confirmation d'adresse mail ne correspond à aucun·e "
                "utilisateur·ice inscrit·e",
            )
            return reverse("mainsite:home")

        if not email_token_generator.check_token(user, token):
            messages.error(
                self.request,
                "Le lien de confirmation d'adresse mail est invalide ou déjà utilisé",
            )
            return reverse("mainsite:home")

        contestant.email_confirmed = True
        contestant.save()
        login(self.request, user)
        messages.info(self.request, "Votre adresse email a bien été confirmée.")
        if hasattr(contestant, "meals"):
            return reverse("registration:registration")
        return reverse("registration:register_meals")


class MealInfosSelectionView(MyRegistrationMixin, UserIsContestantMixin, UpdateView):
    """ Allows a user to select their meals """

    model = MealInfos
    form_class = registration_forms.MealsForm
    template_name = "registration/register/meals.html"

    success_url = reverse_lazy("registration:registration")

    def get_object(self, queryset=None):
        contestant = self.request.user.contestant
        if hasattr(contestant, "meals"):
            return contestant.meals
        else:
            return None

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs["contestant"] = self.request.user.contestant
        return form_kwargs

    @staticmethod
    def get_meals_headtext():
        """ Get the page headtext from the site configuration """
        config = SiteWideSettings.get_solo()
        return mark_safe(markdownify(config.meals_info_headtext))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["meals_headtext"] = self.get_meals_headtext()
        context["meal_selection_fields"] = (
            MealInfos.BREAKFASTS + MealInfos.HOT_MEALS + MealInfos.BRUNCHS
        )
        context["breakfast_fields"] = MealInfos.BREAKFASTS
        context["hot_meal_fields"] = MealInfos.HOT_MEALS
        context["brunch_fields"] = MealInfos.BRUNCHS
        return context


class UpdatePersonalInfosView(MyRegistrationMixin, UserIsContestantMixin, UpdateView):
    template_name = "registration/update_personal_infos.html"
    form_class = registration_forms.PersonalInfosForm

    def get_object(self):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["change_password_form"] = registration_forms.UpdatePasswordForm(
            user=self.request.user
        )
        return context

    def get_success_url(self):
        if not self.request.user.contestant.email_confirmed:
            return reverse("registration:email_confirmation_needed")
        return reverse("registration:registration")

    def form_valid(self, form):
        messages.info(self.request, "Informations personnelles mises à jour")
        return super().form_valid(form)


class PasswordChangeView(MyRegistrationMixin, UserIsContestantMixin, FormView):
    """ Change a user's password """

    template_name = "registration/change_password.html"
    form_class = registration_forms.UpdatePasswordForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.apply()
        messages.info(self.request, "Mot de passe mis à jour")
        return redirect("registration:registration")


class JoinTeamView(RegistrationMixin, MyRegistrationMixin, TemplateView):
    """ Join a team through an invite link """

    template_name = "registration/join_team.html"
    template_name_on_error = "registration/join_team.error.html"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.team_token = None
        self.team = None

    def set_team(self):
        self.team_token = self.kwargs.get("invite_token")
        try:
            self.team = Team.objects.get(invite_token=self.team_token)
        except Team.DoesNotExist:
            self.team = None

    def should_ignore(self):
        if self.request.user.is_authenticated:
            if (
                hasattr(self.request.user, "contestant")
                and self.request.user.contestant.team
                and self.request.user.contestant.team == self.team
            ):
                return True
        return False

    def get_template_names(self):
        """ Return error template upon invalid token """
        if self.team is None:
            return [self.template_name_on_error]
        return super().get_template_names()

    def redirect_after_operation(self):
        """ Redirect the user out of this form to a relevant location """
        if self.request.user.is_authenticated:
            return redirect("registration:registration")
        return redirect("registration:register_start")

    def get(self, request, *args, **kwargs):
        self.set_team()

        if self.should_ignore():
            return self.redirect_after_operation()

        response = super().get(request, *args, **kwargs)
        if self.team is None:
            response.status_code = 404
        return response

    def post(self, request, *args, **kwargs):
        """ Handle button click """
        self.set_team()

        join_type = request.POST["join_type"]
        if join_type == "cancel":
            return self.redirect_after_operation()
        if join_type == "join_and_leave":
            contestant = self.request.user.contestant
            old_team = self.request.user.contestant.team
            contestant.team = self.team
            contestant.save()
            old_team.garbage_collect()
            messages.info(self.request, "Vous avez bien changé d'équipe.")
            return self.redirect_after_operation()
        if join_type == "join_team":
            contestant = self.request.user.contestant
            contestant.team = self.team
            contestant.save()
            messages.info(self.request, "Vous avez bien rejoint votre nouvelle équipe.")
            return self.redirect_after_operation()
        if join_type == "login":
            return redirect(
                reverse("registration:login") + "?next={}".format(self.request.path)
            )
        if join_type == "register":
            self.request.session["register_team_type"] = "join_team"
            self.request.session["register_team_join_token"] = self.team_token
            return redirect("registration:register_infos")

        messages.error(
            self.request,
            (
                "Une erreur est survenue lors du traitement. Avez-vous cliqué "
                "sur l'un des boutons ?"
            ),
        )
        return redirect(self.request.path)

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated and not hasattr(
            self.request.user, "contestant"
        ):  # scabuche special case — some admin is trolling.
            raise PermissionDenied

        context = super().get_context_data(**kwargs)
        context["team"] = self.team
        return context


class RegistrationView(MyRegistrationMixin, UserIsContestantMixin, TemplateView):
    template_name = "registration/registration.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        config = SiteWideSettings.get_solo()
        context["undercrowded_threshold"] = config.undercrowded_threshold
        context["overcrowded_threshold"] = config.overcrowded_threshold
        return context


class TeamView(UserIsContestantMixin, TemplateView):
    template_name = "registration/team.html"

    def post(self, request, *args, **kwargs):
        action = request.POST["action"]
        contestant = request.user.contestant

        if action == "create_team":
            if contestant.team:
                raise PermissionDenied
            team = Team.objects.create()
            contestant.team = team
            contestant.save()
            messages.info(self.request, "Vous avez bien créé une nouvelle équipe")
            return self.get(request, *args, **kwargs)
        elif action == "leave_team":
            if not contestant.team:
                raise PermissionDenied
            old_team = self.request.user.contestant.team
            contestant.team = None
            contestant.save()
            old_team.garbage_collect()
            messages.info(self.request, "Vous avez bien quitté l'équipe.")
            return self.get(request, *args, **kwargs)
        elif action == "nop":
            return redirect("registration:registration")
        raise PermissionDenied

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        config = SiteWideSettings.get_solo()
        context["undercrowded_threshold"] = config.undercrowded_threshold
        context["overcrowded_threshold"] = config.overcrowded_threshold
        return context
