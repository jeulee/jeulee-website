# Generated by Django 3.0.2 on 2020-02-01 22:32

from django.db import migrations, models
import registration.models


class Migration(migrations.Migration):

    dependencies = [
        ("registration", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="team",
            name="invite_token",
            field=models.SlugField(
                default=registration.models.generate_invite_token,
                max_length=10,
                null=True,
            ),
        ),
    ]
